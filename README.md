# Front-End Landing page

![Home page](https://www.dropbox.com/s/80227i960io6qcd/Screenshot_2020-02-07%20Krilatko.jpg?dl=1)

# Front-End Responsive

![Mobile](https://www.dropbox.com/s/2rvcixi1smsolab/Screenshot_2020-02-07%20Krilatko.png?dl=1)

[![Dependabot badge](https://flat.badgen.net/dependabot/wbkd/webpack-starter?icon=dependabot)](https://dependabot.com/)

A lightweight foundation for your next webpack based frontend project.


### Installation

```
npm install
```

### Start Dev Server

```
npm start
```

### Build Prod Version

```
npm run build
```

### Features:

* ES6 Support via [babel](https://babeljs.io/) (v7)
* SASS Support via [sass-loader](https://github.com/jtangelder/sass-loader)
* Linting via [eslint-loader](https://github.com/MoOx/eslint-loader)

When you run `npm run build` we use the [mini-css-extract-plugin](https://github.com/webpack-contrib/mini-css-extract-plugin) to move the css to a separate file. The css file gets included in the head of the `index.html`.
