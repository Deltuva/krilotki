import 'bootstrap';
import '../styles/app.sass';
import counterUp from 'counterup2';
import '../scripts/modules/scrollup';

const numWithSpaces = (num) => {
  return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
};

const countUp = () => {
  const elCounters = document.querySelectorAll("span.counter");
  elCounters.forEach(counter => {
    let countNumber = counter.textContent;
    counter.textContent = numWithSpaces(countNumber);

    counterUp(counter, {
      duration: 1000,
      delay: 10,
    });
  });
};

// init
countUp();